var conf = require('./config');

var rooms = [];

function GetById(r, idx){
    let ret = null;
    let id = parseInt(idx);
    for(let i = 0; i < r.length; i++){
        if(r[i].id === id) {
            ret = r[i];
        }
    }
    return ret;
}

function AddUser(ro, user, id, update_call) {
    let r = GetById(ro, id);
    if(r === null) {
        console.log(`>>>>> [ERROR] PRODUCT ${id} HIT By [${user.email}] Was SOLD!`);
        return false;
    } else {
        if(r.isSold) {
            console.log(`>>>>> [ERROR] PRODUCT ${id} HIT By [${user.email}] Was SOLD to [${r.users}]! ${r.hit}]`);
            return false;
        }
    }

    r.hit += 1;
    let room_price = r.default_price;
    let hit = r.hit;
    let price_user = room_price - (hit * conf.coins);

    let dt = {
        id: user.user_id,
        price: price_user
    }

    
    r.users.push(dt);
    let idx = ro.findIndex(v => v.id === r.id);
    //rooms[idx] = r;
    update_call(idx, r);
    console.log(`>>>>> PRODUCT ${id} ${r.title_eng} Lirik By [${user.email}] Price Down TO RP. ${price_user}`);
    return {
        id: id,
        price: price_user,
        users: r.users
    }
}

function DeleteRooms(ro, user, id, updated_call) {

    let r = GetById(ro, id);
    if(r === null) {
        console.log(`>>>>> [ERROR] PRODUCT ${id} HIT By [${user.email}] Was SOLD!`);
        return false;
    } else {
        if(r.isSold) {
            console.log(`>>>>> [ERROR] PRODUCT ${id} HIT By [${user.email}] Was SOLD to [${r.users}]! [${r.hit}]`);
            return false;
        }
    }

    let usr = r.users.filter(v => v.id === user.user_id);
    let room_price = r.default_price;
    let hit = r.hit;
    let pf = 0;

    if(usr.length > 0) {
        let price_user = usr[usr.length - 1].price; //room_price - (hit * conf.coins);
        pf = price_user;
        console.log(`>>>>> [ATT-CUT] PRODUCT ${id} ${r.title_eng} Bought By [${user.email}] With Cutting Price -${hit}X RP. ${price_user}`);
    } else {
        pf = room_price;
        console.log(`>>>>> [ATT-NORM] PRODUCT ${id} ${r.title_eng} Bought By [${user.email}] With Normal Price RP. ${room_price}`);
    }
    let idx = ro.findIndex(v => v.id === r.id);
    if(idx > -1) {
        r.users = user.email;
        r.current_price = pf;
        r.isSold = true;
        //rooms[idx] = r;
        updated_call(idx, r);
        r.hit = "SOLD";
        return {
            ...r
        };
    } else {
        return false;
    }
}

module.exports = {
    rooms: rooms,
    AddUser: (rooms, user, id, callback) => AddUser(rooms, user, id, callback),
    DeleteRooms: (rooms, user, id, callback) => DeleteRooms(rooms, user, id, callback)
}