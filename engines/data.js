
var data = [
    {
        id: 0,
        name: "SMARTPHONE X",
        price: 1000000,
        users: [],
        count: 0
    },
    {
        id: 1,
        name: "SMARTPHONE Y",
        price: 1000000,
        users: [],
        count: 0
    },
    {
        id: 2,
        name: "SMARTPHONE 0",
        price: 1000000,
        users: [],
        count: 0
    },
    {
        id: 3,
        name: "SMARTPHONE C",
        price: 1000000,
        users: [],
        count: 0
    },
    {
        id: 4,
        name: "SMARTPHONE J",
        price: 1000000,
        users: [],
        count: 0
    },
    {
        id: 5,
        name: "SMARTPHONE V",
        price: 1000000,
        users: [],
        count: 0
    },
    {
        id: 6,
        name: "SMARTPHONE M",
        price: 1000000,
        users: []
    },
    {
        id: 7,
        name: "SMARTPHONE Z",
        price: 1000000,
        users: [],
        count: 0
    },
    {
        id: 8,
        name: "SMARTPHONE S",
        price: 1000000,
        users: [],
        count: 0
    },
    {
        id: 9,
        name: "SMARTPHONE B",
        price: 1000000,
        users: []
    },
    {
        id: 10,
        name: "SMARTPHONE Z DYNAMICS",
        price: 1000000,
        users: [],
        count: 0
    },
    {
        id: 11,
        name: "LAPTOP X",
        price: 1000000,
        users: [],
        count: 0
    },
    {
        id: 12,
        name: "LAPTOP M",
        price: 1000000,
        users: [],
        count: 0
    },
    {
        id: 13,
        name: "LAPTOP S",
        price: 1000000,
        users: [],
        count: 0
    },
    {
        id: 14,
        name: "LAPTOP V",
        price: 1000000,
        users: [],
        count: 0
    },
    {
        id: 15,
        name: "LAPTOP Y",
        price: 1000000,
        users: [],
        count: 0
    },
    {
        id: 16,
        name: "LAPTOP Z",
        price: 1000000,
        users: [],
        count: 0
    },
    {
        id: 17,
        name: "LASER TOUCH",
        price: 1000000,
        users: [],
        count: 0
    }
]

module.exports = data;