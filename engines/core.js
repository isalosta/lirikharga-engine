
var rooms = require('./pool');
var mysql   = require("mysql");
var connection = require('../database');
var BotWatch = require('../bots/watcher');

function InitiateData() {
    let query = "SELECT * FROM ?? ";
    let table = ["product"];

    query = mysql.format(query,table);
    connection.Connection().query(query,function(err,rows){
        if(err) {
            console.log("Error executing MySQL query");
            BotWatch('[ERROR 500] executing MySQL query (STATE: INITIAL DB)');
            return 0;
        } else {
            rooms.rooms = [];
            let len = rows.length;
            for(let i = 0; i < len; i++) {
                let len_usr_id = rows[i].users === null ? [] : rows[i].users.split(",");
                let len_usr_price = rows[i].users_price === null ? [] :rows[i].users_price.split(",");
                let users = [];
                for(let j = 0; j < len_usr_id.length; j++) {
                    users.push({id: parseInt(len_usr_id[i]), price: parseInt(len_usr_price[i])});
                }
                let dt = rows[i];
                dt.users = users;
                dt.users_price = [];
                dt.hit = len_usr_id.length;
                dt.isSold = rows[i].hit === 1 ? true : false;
                let pushd = {...dt}
                rooms.rooms.push(pushd);
            }
            BotWatch('[REFRESH] Updated Stream');
            return 1;
        }
    });
}

function SetRoomData(idx, updated_data) {
    rooms[idx] = updated_data;
}

async function Lirik(u, id) {
    let user = u;

    if(!user) {
        return {status: 0, message: "Unauthorize"}
    }

    if(user.coin > 0) {
        let conn = await connection.Async();
        let query = "SELECT * FROM user WHERE user_id=?";
        let table = [user.user_id];
        query = mysql.format(query,table);
        let usr = false;
        try {
            let row = await conn.query(query);
            usr = row[0];
        } catch (err) {
            console.log(err, "Error executing MySQL query");
            BotWatch('ERROR 500] executing MySQL query (STATE: LIRIK-1)');
            return({status: 500, message: "database Error"});
        }
        if(usr) {
            if(usr.coin > 0) {
                query = "UPDATE user SET coin=? WHERE user_id=?";
                table = [usr.coin - 1, user.user_id];
                query = mysql.format(query,table);
                
                try {
                    let row = await conn.query(query);
                    let result = rooms.AddUser(rooms.rooms, user, id, SetRoomData);
                    //console.log(result);
                        if(result) {
                            BotWatch(`[LIRIK] PRODUCT ${id} by [${user.email} || Coin: ${usr.coin - 1}] Price Down TO RP. ${result.price}`);
                            let i_usr = [];
                            let p_usr = [];
                            for(let i = 0; i < result.users.length; i++) {
                                i_usr.push(result.users_price[i].id);
                                p_usr.push(result.users[i].price);
                            }
                            let query2 = "UPDATE product SET users=?, users_price=? WHERE id=?";
                            let table2 = [i_usr.join(","),p_usr.join(","), id];
                           // console.log(table2);
                            query2 = mysql.format(query2,table2);
                            let row2 = await conn.query(query2);
                            return({status: 200, id: result.id, price: result.price, coin: usr.coin - 1});
                        } else {
                            return({status: 0, message: "product Sold"});
                        }
                } catch (err) {
                    console.log(err, "Error executing MySQL query");
                    BotWatch('[ERROR 500] Executing MySQL query (STATE: LIRIK-2 COIN)');
                    return({status: 500, message: "Internal Server Error"});
                }
            } else {
                BotWatch(`[ERR LIRIK] PRODUCT ${id} by [${user.email} || Coin: ${usr.coin - 1}] Have No Coin!`);
                return({status: 0, message: "no coin"});
            }
        } else {
            BotWatch(`[FATAL][ERR LIRIK] PRODUCT ${id} Forced By Unauthorized User!`);
            return({status: 0, message: "no user data"});
        }
    }
}

async function Buy(user, id) {
    let result = rooms.DeleteRooms(rooms.rooms, user, id, SetRoomData);
    if(result) {
        BotWatch(`[SOLD] PRODUCT ${id} by [${user.email}] RP. ${result.current_price}`);
        let conn = connection.Async();
        let query = "UPDATE product SET hit=? WHERE id=?";
        let table = [1, id];
        query = mysql.format(query,table);
        try {
            let row = await conn.query(query);
            return {status: 1, ...result}
        } catch (err) {
            console.log(err, "Error executing MySQL query (STATE: BUY)");
            BotWatch('[ERROR 500] Executing MySQL query (STATE: BUY)');
            return({status: 0, message: "Buy Error"});
        }
    } else {
        BotWatch(`[ERR SOLD] PRODUCT ${id} Was SOLD by [${user.email}]`);
        return {status: 0, message: "Product Sold"}
    }
}

async function BuyCoin(user, amount) {
    if(!user) {
        return {status: 0, message: "Unauthorize"}
    }

    let conn = await connection.Async();

    let query = "SELECT * FROM user WHERE user_id=?";
    let table = [user.user_id];
    query = mysql.format(query,table);
    let usr = false;

    try {
        let row = await conn.query(query);
        usr = row[0];
    } catch (err) {
        console.log(err, "Error executing MySQL query");
        BotWatch('[ERROR 500] Executing MySQL query (STATE: BUYCOIN)');
        return({status: 500, message: "database Error"});
    }

    query = "UPDATE user SET coin=? WHERE user_id=?";
    table = [usr.coin + amount, user.user_id];
    query = mysql.format(query,table);

    try {
        let row = await conn.query(query);
        let dt = user;
        dt.coin = usr.coin + amount;
        console.log(`>>>>> [TRX-C] TRANSACTIONS: ${usr.email} Bought [${usr.coin + amount}] Coin!`);
        BotWatch(`[COIN BOUGHT] by [${usr.email}] Current Coin: ${usr.coin + amount}`);
        return {status: 1, data: dt};
    } catch (err) {
        console.log(err);
        BotWatch(`[ERR BUY COIN] by [${usr.email}]`);
        return {status: 0, message: "Buy Coin Failed"}
    }
}

function GetHome() {
    return {
        data: rooms.rooms
    }
}

function GetById(idx){
    let ret = null;
    let id = parseInt(idx);
    for(let i = 0; i < rooms.rooms.length; i++){
        if(rooms.rooms[i].id === id) {
            ret = rooms.rooms[i];
        }
    }
    return ret;
}

function GetProductDetails(id) {
    let d = GetById(id);
    return {
        data: d
    }
}

function CheckStatus(id) {
    let r = GetById(id);
    if(r === null) {
        return {
            status_product: 0
        }
    } else {
        if(r.isSold) {
            return {
                status_product: 0
            }
        } else {
            return {
                status_product: 1
            }
        }
    }
}

module.exports = {
    InitiateData,
    Lirik,
    Buy,
    GetHome,
    BuyCoin,
    GetProductDetails,
    CheckStatus
}
