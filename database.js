var mysql = require('mysql');
const util = require( 'util' );

//var connectionString = 'mysql://root:password@localhost/lirikharga?charset=utf8';
//var connectionString = "mysql://XlbvF2zZXY:YtD14rz4Jd@remotemysql.com/XlbvF2zZXY?charset=utf8";
var connectionString = "mysql://lirikharga_admin:BijiKudaP0N!@lirikharga-db.crdmzz13jwdw.ap-southeast-1.rds.amazonaws.com/lirikdb?charset=utf8"
var connection = mysql.createConnection(connectionString);

connection.connect(function(err) {
    if (err){
        console.log(err);
        connection.end;
    }
});

function Conn() {
    return connection;
}

function AsyncDb() {
    const conn = connection; //mysql.createConnection(connectionString);  
    return {
      query( sql, args ) {
        return util.promisify( conn.query )
          .call( conn, sql, args );
      },
      close() {
        return util.promisify( conn.end ).call( conn );
      }
    };
}
 
module.exports = {Connection: () => Conn(), Async: () => AsyncDb()};