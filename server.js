var express = require("express");
var bodyParser  = require("body-parser");
var cors = require('cors');
var md5 = require("MD5");

var botWatch = require('./bots/watcher');
var db = require('./database');
var status = require('./status');
var core = require('./engines/core');

var verifyToken = require('./middleware/verifyToken');
var addNewUser = require('./middleware/addNewUser');
var userLoginCheck = require('./middleware/userLoginCheck');
var findAllUsers = require('./middleware/findAllUsers');
var findAllProducts = require('./middleware/findAllProduct');
var welcome = require('./middleware/welcome');

var changePassword = require('./middleware/changeUserPassword');
var deleteSession = require('./middleware/deleteSessionId');
var blockUser = require('./middleware/blockUser');

var port = process.env.PORT || 56011;
var app  = express();

async function init (){
    let conn = db.Async();
    let query = "SELECT * FROM status";
    try {
        let row = await conn.query(query);
        let dt = row[0].status;
        if(dt === 1) {
            status.set(dt);
            console.log(`STATUS ${status.getStatus()}: ${status.getMessage()}`)
            botWatch(`STATUS ${status.getStatus()}: ${status.getMessage()}`);
        } else {
            status.set(dt);
            botWatch(`STATUS ${status.getStatus()}: ${status.getMessage()}`);
        }
    } catch {
        status.set(3);
        botWatch(`STATUS ${status.getStatus()}: ${status.getMessage()}`);
    }
}

init().then(r => {
    core.InitiateData();
}).catch(err =>{
    console.log(err);
})

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.use(cors());

app.post('/signup', addNewUser);
app.post('/login', (req, res, next) => {
    userLoginCheck.userLoginCheck(req).then(result => {
        res.send(result);
    }).catch(err => {
        res.send(err);
    }) 
});

app.get('/loaderio-bf3130e167029b78575d5052c20fcc88', (req, res, next) => {
    res.setHeader('Content-disposition', 'attachment; filename=loaderio-bf3130e167029b78575d5052c20fcc88.txt');
    res.setHeader('Content-type', 'text/plain');
    res.charset = 'UTF-8';
    res.write("loaderio-bf3130e167029b78575d5052c20fcc88");
    res.end();
})

app.post('/forgot_password', (req, res, next) => {
    changePassword(req.body.password, req.body.email).then(response => {
        if(response) {
            botWatch(`[FORGOT] Change Password For ${req.body.email}`);
            res.send({status: 200, message: "ok"});
        } else {
            botWatch(`[FORGOT ERROR] DB Error => Change Password For ${req.body.email}:1`);
            res.send({status: 400, message: "error"});
        }
    }).catch(err => {
        botWatch(`[FORGOT ERROR] DB Error => Change Password For ${req.body.email}:2`);
        res.send({status: 500, message: err});
    }) 
});

app.get('/home', (req, res) => {
    if(status.get() === 1) {
        res.send(core.GetHome());
    } else {
        res.send({status: status.getStatus(), message: status.getMessage()})
    }
})

app.get('/product/:id', (req, res) => {
    if(status.get() === 1) {
        res.send(core.GetProductDetails(req.params.id));
    } else {
        res.send({status: status.getStatus(), message: status.getMessage()})
    }
});

app.get('/product_status/:id', (req, res) => {
    if(status.get() === 1) {
        res.send({status: 200, ...core.CheckStatus(req.params.id)});
    } else {
        res.send({status: status.getStatus(), message: status.getMessage()})
    }
})

////////////////////////////////////////////////////////////////////////
var apiRoutes = express.Router();
apiRoutes.use(bodyParser.urlencoded({ extended: true }));
apiRoutes.use(bodyParser.json());
apiRoutes.use((req, res, next) => {
    if(status.get() === 1) {
        next();
    } else {
        res.send({status: status.getStatus(), message: status.getMessage()})
    }
})
apiRoutes.use(verifyToken);
apiRoutes.get('/', welcome);
//apiRoutes.get('/users', findAllUsers);

apiRoutes.all('/me/profile', (req, res, next) => {
    res.send({
        ...req.currUser['0']
    })
})

apiRoutes.post('/lirik/:id', function(req, res) {
    core.Lirik(req.currUser['0'], req.params.id).then(result => {
        res.send(result);
    }).catch(err => res.status(500).send({message: err}));
});

apiRoutes.post('/buy/:id', function(req, res) {
    core.Buy(req.currUser['0'], req.params.id).then(result => {
        res.send(result);
    }).catch(err => {
        res.status(500).send({message: err});
    });
});

apiRoutes.post('/buycoins', (req, res) => {
    core.BuyCoin(req.currUser['0'], req.body.amount).then(result => {
        res.send(result);
    }).catch(err => {
        res.status(500).send({message: err});
    });
})
////////////////////////////////////////////////////////////////////////////

var apiMaster = express.Router();
apiMaster.use(bodyParser.urlencoded({ extended: true }));
apiMaster.use(bodyParser.json());
apiMaster.use((req, res, next) => {
    let token = req.body.token || req.query.token || req.headers['token'];
    if(token === md5('whosyourdaddy')) {
        next();
    } else {
        res.send({status: 401, message: "Unauthorize"});
    }
});

apiMaster.get('/', welcome);
apiMaster.get('/users', findAllUsers);
apiMaster.get('/products', findAllProducts);

apiMaster.get('/status', (req, res, next) => {
    res.send({status: status.getStatus(), message: status.getMessage()})
})

apiMaster.post('/update_status', (req, res, next) => {
    let conn = db.Async();
    let query = `UPDATE status SET status=${req.body.status} WHERE id=1`;
    try {
        let row = conn.query(query);
        status.set(req.body.status)
        botWatch(`[UPDATED] STATUS ${status.getStatus()}: ${status.getMessage()}`);
        res.send({status: 200, message: "ok"});
    } catch(err) {
        status.set(3);
        botWatch(`[ERROR 500] Mysql ERROR!`);
        botWatch(`[UPDATED] STATUS ${status.getStatus()}: ${status.getMessage()}`);
        res.send({status: 500, message: "server error"});
    }
});

apiMaster.post('/change_password', (req, res, next) => {
    changePassword(req.body.password, req.body.email).then(response => {
        if(response) {
            botWatch(`[ADMIN MODE] Change Password For ${req.body.email}`);
            res.send({status: 200, message: "ok"});
        } else {
            botWatch(`[ADMIN MODE ERROR] DB Error => Change Password For ${req.body.email}:1`);
            res.send({status: 400, message: "error"});
        }
    }).catch(err => {
        botWatch(`[ADMIN MODE ERROR] DB Error => Change Password For ${req.body.email}:2`);
        res.send({status: 500, message: err});
    }) 
});

apiMaster.post('/block_user', (req, res, next) => {
    blockUser(req.body.email, 1).then(response => {
        if(response) {
            botWatch(`[ADMIN MODE] Block User ${req.body.email}`);
            res.send({status: 200, message: "ok"});
        } else {
            botWatch(`[ADMIN MODE ERROR] DB Error => Block User ${req.body.email}:1`);
            res.send({status: 400, message: "error"});
        }
    }).catch(err => {
        botWatch(`[ADMIN MODE ERROR] DB Error => Block User ${req.body.email}:2`);
        res.send({status: 500, message: err});
    }) 
});

apiMaster.post('/unblock_user', (req, res, next) => {
    blockUser(req.body.email, 0).then(response => {
        if(response) {
            botWatch(`[ADMIN MODE] Unblock User ${req.body.email}`);
            res.send({status: 200, message: "ok"});
        } else {
            botWatch(`[ADMIN MODE ERROR] DB Error => UNlock User ${req.body.email}:1`);
            res.send({status: 400, message: "error"});
        }
    }).catch(err => {
        botWatch(`[ADMIN MODE ERROR] DB Error => unblock User ${req.body.email}:2`);
        res.send({status: 500, message: err});
    }) 
});

apiMaster.post('/delete_seesion', (req, res, next) => {
    deleteSession(req.body.id).then(response => {
        if(response) {
            botWatch(`[ADMIN MODE] Delete Seesion By USER ID ${req.body.id}`);
            res.send({status: 200, message: "ok"});
        } else {
            botWatch(`[ADMIN MODE ERROR] DB Error => Delete Session User ID ${req.body.id}:1`);
            res.send({status: 400, message: "error"});
        }
    }).catch(err => {
        botWatch(`[ADMIN MODE ERROR] DB Error => Delete Session User ID  ${req.body.id}:2`);
        res.send({status: 500, message: err});
    });
});

apiMaster.post('/add_coin_user', (req, res) => {
    core.BuyCoin({user_id: req.body.id, coin: 0}, req.body.amount).then(result => {
        res.send(result);
    }).catch(err => {
        res.status(500).send({message: err});
    });
});

apiMaster.post('/force_lirik', function(req, res) {
    core.Lirik({user_id: req.body.user_id, email: "[ADMIN FORCE]", coin: 1}, req.body.product_id).then(result => {
        res.send(result);
    }).catch(err => res.status(500).send({message: err}));
});

////////////////////////////////////////////////////////////////////////////

app.use('/api', apiRoutes);
app.use('/master', apiMaster);

////////////////////////////////////////////////////////////////////////////

app.listen(port, function() {
    console.log('Express server listening on port ' +port);
});
