'use strict';

var Axios = require('axios');

let token = "1012371419:AAFEVtzwScIaWRULjLXEAUXTBHMgPm-nkRI";
let chat_id = "-234729993";

function SendMessage(msg) {
    let date = new Date();
    date = `${date.getDate() > 10 ? date.getDate() : '0' + date.getDate()}/${date.getMonth() > 10 ? date.getMonth() : '0' + date.getMonth()}/${date.getFullYear()} ${date.getHours() > 10 ? date.getHours() : '0' + date.getHours()}:${date.getMinutes() > 10 ? date.getMinutes() : '0'+date.getMinutes()}:${date.getSeconds() > 10 ? date.getSeconds() : '0'+date.getSeconds()}`;
    let message = encodeURIComponent(`${date} ${msg}`);
    Axios.default.get(`https://api.telegram.org/bot${token}/sendMessage?chat_id=${chat_id}&text=${message}`);
}

module.exports = (msg) => SendMessage(msg)