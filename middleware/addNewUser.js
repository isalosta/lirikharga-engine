
var mysql   = require("mysql");
var md5 = require("MD5");
var connection = require("../database");
var BotWatch = require('../bots/watcher');

 var addNewUser = (req,res, next) => {
 	var date = new Date();
    var post  = {
      full_name:req.body.full_name,
      email:req.body.email,
      password:md5(req.body.password),
      dob:req.body.dob,
      device_type:req.body.device_type,
	  device_token:req.body.device_token,
	  image_path:req.body.image_path,
	  address:req.body.address,
	  city:req.body.city,
	  phone_number:req.body.phone_number,
	  coin: 5
	};
	  
	var query = "SELECT email FROM ?? WHERE ??=?";
	var table = ["user", "email", post.email];
	query = mysql.format(query,table);

	connection.Connection().query(query,function(err,rows){

		if(err) {
			console.log("error DB");
			BotWatch('[ERROR 500] Error executing MySQL query [REGISTER -1]');
			res.json({"status" : 500, "message" : "Error executing MySQL query"});
		}
		else {
			if(rows.length==0){
				var query = "INSERT INTO  ?? SET  ?";
				var table = ["user"];
				query = mysql.format(query,table);
				connection.Connection().query(query, post, function(err,rows){
					if(err) {
						console.log("Q ERR", err);
						BotWatch('[ERROR 500] Error executing MySQL query [REGISTER -2]');
						res.json({"status" : 500, "message" : "Error executing MySQL query"});
					} else {
						BotWatch(`[REGISTER] New Registered User [${post.email}]`);
						res.json({"status" : 200, "message" : "Success"});
					}
				});
			}
			else{
				console.log("ROWS LENGTH", rows.length);
				res.json({"status" : 201, "message" : "Email Id already registered"});
			}
		}
	});
}

   module.exports = addNewUser;


