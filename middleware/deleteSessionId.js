var connection = require("../database");
var mysql   = require("mysql");

async function DeleteSession (id) {
    let conn = connection.Async();
    let query = `DELETE FROM access_token WHERE user_id = ?`;
    query = mysql.format(query, [id])
    try {
        let row = await conn.query(query);
        return true;
    } catch (err) {
        console.log("ERROR DB", err);
        return false;
    }
}

module.exports = (id) => DeleteSession(id);