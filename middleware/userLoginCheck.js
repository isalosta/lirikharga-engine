
var mysql   = require("mysql");
var md5 = require("MD5");
var jwt = require('jsonwebtoken'); // used to create, sign, and verify tokens
var config = require('../config');
var connection = require("../database"); // get our config file
var Axios = require('axios');
var BotWatch = require('../bots/watcher');


var userLoginCheck = async function (req) {
	var post  = {
		password:req.body.password,
		email:req.body.email
	}

	var query = "SELECT * FROM ?? WHERE ??=? AND ??=?";
	var table = ["user","password",  md5(post.password), "email", post.email];
	query = mysql.format(query,table);
	var conn = connection.Async();
	let usr = [false];

	try {
		let row = await conn.query(query);
		usr = row;
	} catch(err) {
		console.log(err);
		BotWatch('[ERROR 500] Executing MySQL query [ADD USER]');
		return {"status" : 500, "message" : "Error executing MySQL query [ADD USER]"};
	}

	if(usr[0]) {
		let token = jwt.sign(usr, config.secret, {
			expiresIn: 14400
		});
		user_id= usr[0].user_id;
		let data  = {
			user_id:usr[0].user_id,
			device_type:usr[0].device_type,
			access_token:token,
			device_token:usr[0].device_token,
			ip_address:usr[0].ip_address
		}
		let q = "INSERT INTO  ?? SET  ?";
		let table = ["access_token"];
		let user_data = {
			full_name: usr[0].full_name,
			coin: usr[0].coin
		}
		q = mysql.format(q,table);
		let tkn = false;

		try {
			let row = await conn.query(q, data);
			tkn = row;
		} catch (err) {
			console.log(err);
			BotWatch("[ERROR 500] Error executing MySQL query [INSERT ACCESS TOKEN]");
			return {"status" : 500, "message" : "Error executing MySQL query [INSERT ACCESS TOKEN]"};
		}

		if(tkn) {
			BotWatch(`[LOGIN] [${usr[0].email}]`);
			return {
				success: true,
				message: 'Token generated',
				token: token,
				currUser: user_id,
				data: user_data
			};
		} else {
			return {"status" : 401, "message" : "wrong email/password combination"};
		}
	} else {
		return {"status" : 401, "message" : "wrong email/password combination"};
	}

	// 	connection.Connection().query(q, data, function(err,rows){
	// 		if(err) {
	// 			res.json({"Error" : true, "Message" : "Error executing MySQL query"});
	// 		} else {
	// 			res.json({
	// 				success: true,
	// 				message: 'Token generated',
	// 				token: token,
	// 				currUser: user_id,
	// 				data: user_data
	// 			});
	// 		} // return info including token
	// 	});
	// } else {
	// 	res.json({"status" : 401, "Message" : "wrong email/password combination"});
	// }

	// connection.Connection().query(query,function(err,rows){
	// 	if(err) {
	// 		res.json({"Error" : true, "Message" : "Error executing MySQL query"});
	// 	}
	// 	else {
	// 		if(rows.length==1){
	// 			var token = jwt.sign(rows, config.secret, {
	// 				expiresIn: 14400
	// 			});
	// 			user_id= rows[0].user_id;
	// 			var data  = {
	// 				user_id:rows[0].user_id,
	// 				device_type:rows[0].device_type,
	// 				access_token:token,
	// 				device_token:rows[0].device_token,
	// 				ip_address:rows[0].ip_address
	// 			}
	// 			var query = "INSERT INTO  ?? SET  ?";
	// 			var table = ["access_token"];
	// 			let user_data = {
	// 				first_name: rows[0].first_name,
	// 				last_name: rows[0].last_name,
	// 				coin: rows[0].coin
	// 			}
	// 			query = mysql.format(query,table);
	// 			connection.Connection().query(query, data, function(err,rows){
	// 				if(err) {
	// 					res.json({"Error" : true, "Message" : "Error executing MySQL query"});
	// 				} else {
	// 					res.json({
	// 						success: true,
	// 						message: 'Token generated',
	// 						token: token,
	// 						currUser: user_id,
	// 						data: user_data
	// 					});
    //        			} // return info including token
    //        		});
	// 		}
	// 		else {
	// 			res.json({"Error" : true, "Message" : "wrong email/password combination"});
	// 		}

	// 	}
	// });
}

module.exports = { userLoginCheck: (req) => userLoginCheck(req)};
