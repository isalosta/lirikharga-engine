var connection = require("../database");

async function BlockUser(email, opt) {
    let conn = connection.Async();
    let query = `UPDATE user SET block_status=${opt} WHERE user_id=${email}`;
    try {
        let row = await conn.query(query);
        return true;
    } catch (err) {
        console.log("ERROR DB", err);
        return false;
    }
}

module.exports = (email, opt) => BlockUser(email, opt);