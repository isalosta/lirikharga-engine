var connection = require("../database");

var findAllProducts = function (req, res) {
	var query = "SELECT * FROM product";
    connection.Connection().query(query,function(err,rows){
        if(err) {
            res.json({"status" : 500, "message" : "Error executing MySQL query"});
        } else {
            res.json({"status" : 200, "message" : "Success", "products" : rows});
        }
    });
};
module.exports = findAllProducts;