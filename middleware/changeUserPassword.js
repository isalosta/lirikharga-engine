var connection = require("../database");
var mysql   = require("mysql");
var md5 = require('MD5');

async function ChangePassword (pass, email) {
    let conn = connection.Async();
    let password = md5(pass);
    let query = `UPDATE user SET password=? WHERE user_id=?`;
    query = mysql.format(query, [password, email]);
    try {
        let row = await conn.query(query);
        return true;
    } catch (err) {
        console.log("ERROR DB", err);
        return false;
    }
}

module.exports = (pass, email) => ChangePassword(pass, email);