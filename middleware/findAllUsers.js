
var connection = require("../database");

var findAllUsers = function (req, res) {
	var query = "SELECT * FROM user";
    connection.Connection().query(query,function(err,rows){
        if(err) {
            res.json({"Error" : 500, "message" : "Error executing MySQL query"});
        } else {
            res.json({"status" : 200, "message" : "Success", "users" : rows});
        }
    });
};
module.exports = findAllUsers;