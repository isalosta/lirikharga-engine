'use strict'

var stats = 1;

function setStats(st) {
    stats = st;
}

function getStats() {
    return stats;
}

function getMessage() {
    switch(stats) {
        case 0:
            return "Server is Maintenance, Please Try Again Later";
        case 2:
            return "Server is Under Construction";
        case 3:
            return "Server is Disabled";
        case 4:
            return "Database Server is Error";
        default:
            return "Server OK";
    }
}

function getStatus() {
    switch(stats) {
        case 0:
            return 202;
        case 2:
            return 501;
        case 3:
            return 500;
        default:
            return 200;
    }
}

module.exports = {
    set: (st) => setStats(st),
    get: () => getStats(),
    getMessage: () => getMessage(),
    getStatus: () => getStatus()
}